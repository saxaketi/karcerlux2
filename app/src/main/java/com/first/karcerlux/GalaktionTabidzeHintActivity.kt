package com.first.karcerlux
import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Button
import kotlinx.android.synthetic.main.activity_galaktion_tabidze_hint.*
import kotlinx.android.synthetic.main.activity_galaktion_tabidze_hint.buttonA
import kotlinx.android.synthetic.main.activity_galaktion_tabidze_hint.buttonB
import kotlinx.android.synthetic.main.activity_galaktion_tabidze_hint.buttonC
import kotlinx.android.synthetic.main.activity_galaktion_tabidze_hint.buttonD
import kotlinx.android.synthetic.main.activity_galaktion_tabidze_hint.buttonDel
import kotlinx.android.synthetic.main.activity_galaktion_tabidze_hint.buttonE
import kotlinx.android.synthetic.main.activity_galaktion_tabidze_hint.buttonF
import kotlinx.android.synthetic.main.activity_galaktion_tabidze_hint.buttonG
import kotlinx.android.synthetic.main.activity_galaktion_tabidze_hint.buttonH
import kotlinx.android.synthetic.main.activity_galaktion_tabidze_hint.buttonI
import kotlinx.android.synthetic.main.activity_galaktion_tabidze_hint.buttonJ
import kotlinx.android.synthetic.main.activity_galaktion_tabidze_hint.buttonK
import kotlinx.android.synthetic.main.activity_galaktion_tabidze_hint.buttonL
import kotlinx.android.synthetic.main.activity_galaktion_tabidze_hint.buttonM
import kotlinx.android.synthetic.main.activity_galaktion_tabidze_hint.buttonN
import kotlinx.android.synthetic.main.activity_galaktion_tabidze_hint.buttonO
import kotlinx.android.synthetic.main.activity_galaktion_tabidze_hint.buttonP
import kotlinx.android.synthetic.main.activity_galaktion_tabidze_hint.buttonQ
import kotlinx.android.synthetic.main.activity_galaktion_tabidze_hint.buttonR
import kotlinx.android.synthetic.main.activity_galaktion_tabidze_hint.buttonS
import kotlinx.android.synthetic.main.activity_galaktion_tabidze_hint.buttonShift
import kotlinx.android.synthetic.main.activity_galaktion_tabidze_hint.buttonT
import kotlinx.android.synthetic.main.activity_galaktion_tabidze_hint.buttonU
import kotlinx.android.synthetic.main.activity_galaktion_tabidze_hint.buttonV
import kotlinx.android.synthetic.main.activity_galaktion_tabidze_hint.buttonW
import kotlinx.android.synthetic.main.activity_galaktion_tabidze_hint.buttonX
import kotlinx.android.synthetic.main.activity_galaktion_tabidze_hint.buttonY
import kotlinx.android.synthetic.main.activity_galaktion_tabidze_hint.textView1
import kotlinx.android.synthetic.main.activity_galaktion_tabidze_hint.textView2
import kotlinx.android.synthetic.main.activity_galaktion_tabidze_hint.textView3
import kotlinx.android.synthetic.main.activity_galaktion_tabidze_hint.textView4
import kotlinx.android.synthetic.main.activity_galaktion_tabidze_hint.textView5
import kotlinx.android.synthetic.main.activity_galaktion_tabidze_hint.textView6
import kotlinx.android.synthetic.main.activity_galaktion_tabidze_hint.textView7
import kotlinx.android.synthetic.main.activity_galaktion_tabidze_hint.textView8

class GalaktionTabidzeHintActivity : AppCompatActivity() {
    private var firstCharacter = true
    private var firstCharacterOne = true
    private var firstCharacterSecond = true
    private var firstCharacterThird = true
    private var firstCharacterFourth = true
    private var firstCharacterFifth = true
    private var firstCharacterSixth = true
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_galaktion_tabidze_hint)
        restart.setOnClickListener {
            val intent = Intent(this, StartActivity::class.java)
            startActivity(intent)

        }
        init()
    }
    private fun init(){
        buttonQ.setOnClickListener {
            changeText(buttonQ)
        }
        buttonW.setOnClickListener {
            changeText(buttonW)
        }
        buttonE.setOnClickListener {
            changeText(buttonE)
        }
        buttonR.setOnClickListener {
            changeText(buttonR)
        }
        buttonT.setOnClickListener {
            changeText(buttonT)
        }
        buttonY.setOnClickListener {
            changeText(buttonY)
        }
        buttonU.setOnClickListener {
            changeText( buttonU)
        }
        buttonI.setOnClickListener {
            changeText( buttonI)
        }
        buttonO.setOnClickListener {
            changeText(buttonO)}
        buttonP.setOnClickListener {
            changeText(buttonP)
        }
        buttonA.setOnClickListener {
            changeText(buttonA)
        }
        buttonS.setOnClickListener {
            changeText(buttonS)
        }
        buttonD.setOnClickListener {
            changeText(buttonD)
        }
        buttonF.setOnClickListener {
            changeText(buttonF)
        }
        buttonG.setOnClickListener {
            changeText(buttonG)
        }
        buttonH.setOnClickListener {
            changeText(buttonH)
        }
        buttonJ.setOnClickListener {
            changeText(buttonJ)
        }
        buttonK.setOnClickListener {
            changeText(buttonK)
        }
        buttonL.setOnClickListener {
            changeText(buttonL)
        }
        buttonShift.setOnClickListener {
            shift(buttonShift)
        }
        buttonX.setOnClickListener {
            changeText(buttonX)
        }
        buttonC.setOnClickListener {
            changeText(buttonC)
        }
        buttonV.setOnClickListener {
            changeText(buttonV)
        }
        buttonB.setOnClickListener {
            changeText(buttonB)
        }
        buttonN.setOnClickListener {
            changeText(buttonN)
        }
        buttonM.setOnClickListener {
            changeText(buttonM)
        }
        buttonDel.setOnClickListener {
            delete()
        }


    }
    private fun changeText(button: Button) {
        if (textView1.text.isEmpty()) {
            textView1.text = textView1.text.toString() + button.text.toString()
        } else if (textView2.text.isEmpty()) {
            textView2.text = textView2.text.toString() + button.text.toString()
        } else if (textView3.text.isEmpty()) {
            textView3.text = textView3.text.toString() + button.text.toString()
        } else if (textView4.text.isEmpty()) {
            textView4.text = textView4.text.toString() + button.text.toString()
        } else if (textView5.text.isEmpty()) {
            textView5.text = textView5.text.toString() + button.text.toString()
        } else if (textView6.text.isEmpty()) {
            textView6.text = textView6.text.toString() + button.text.toString()
        } else if (textView7.text.isEmpty()) {
            textView7.text = textView7.text.toString() + button.text.toString()
        } else if (textView8.text.isEmpty()) {
            textView8.text = textView8.text.toString() + button.text.toString()
        }
    }
    private fun delete(){
        var emptyView = ""
        if ((textView1.text.isNotEmpty()) && (textView2.text.isNotEmpty()) && (textView3.text.isNotEmpty()) &&(textView4.text.isNotEmpty()) &&
            (textView5.text.isNotEmpty()) && (textView6.text.isNotEmpty()) && (textView7.text.isNotEmpty()) && (textView8.text.isNotEmpty())){

            textView1.text = emptyView
            textView2.text = emptyView
            textView3.text = emptyView
            textView4.text = emptyView
            textView5.text = emptyView
            textView6.text = emptyView
            textView7.text = emptyView
            textView8.text = emptyView
        }
    }

    private fun shift (button: Button){
        if (button.isClickable()){
            if (firstCharacter){
                buttonW.text =  "წ"
                firstCharacter = false
            }
            else{
                buttonW.text = "ჭ"
                firstCharacter = true
            }
            if (firstCharacterOne){
                buttonR.text =  "რ"
                firstCharacterOne = false
            }
            else{
                buttonR.text = "ღ"
                firstCharacterOne = true
            }
            if (firstCharacterSecond){
                buttonT.text =  "ტ"
                firstCharacterSecond = false
            }
            else{
                buttonT.text = "თ"
                firstCharacterSecond = true
            }
            if (firstCharacterThird){
                buttonS.text =  "ს"
                firstCharacterThird = false
            }
            else{
                buttonS.text = "შ"
                firstCharacterThird = true
            }
            if (firstCharacterFourth){
                buttonJ.text =  "ჯ"
                firstCharacterFourth = false
            }
            else{
                buttonJ.text = "ჟ"
                firstCharacterFourth = true
            }
            if (firstCharacterFifth){
                buttonX.text =  "ზ"
                firstCharacterFifth = false
            }
            else{
                buttonX.text = "ძ"
                firstCharacterFifth = true
            }
            if (firstCharacterSixth){
                buttonV.text =  "ც"
                firstCharacterSixth = false
            }
            else{
                buttonV.text = "ჩ"
                firstCharacterSixth = true
            }
        }



    }

}