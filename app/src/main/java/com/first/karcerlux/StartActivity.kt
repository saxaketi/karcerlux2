package com.first.karcerlux

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import kotlinx.android.synthetic.main.activity_start.*

class StartActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_start)
        init()


    }

    private fun init(){
        startButton.setOnClickListener {
            val intent = Intent(this, HarperLeeActivity::class.java)
            startActivity(intent)
            check()
        }
    }
    private fun check(){

            if (Name.text.isNotEmpty()) {
                startButton.isClickable = false

            } else if (Name.text.isEmpty()) {

                startButton.isClickable = true
            }
        }
    }














